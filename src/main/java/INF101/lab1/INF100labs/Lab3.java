package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        crossSum(12);

    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 7;
        while (i <= n) {
            System.out.println(i);
            i+=7;
        }
    }

    public static void multiplicationTable(int n) {
        for (int rounds = 1; rounds <= n; rounds++) {
            System.out.print(rounds + ": ");
            for(int num = 1; num <= n; num++){
                int result = rounds * num;
                System.out.print(result + " ");
            }
            System.out.println();
        }
        
    }

    public static int crossSum(int num) {
        String stringnum = Integer.toString(num);
        int sum = 0;
        for (int i = 0; i < stringnum.length(); i++){
            int digit = Character.getNumericValue(stringnum.charAt(i));
            sum += digit;
        }
        System.out.println(sum);
        return sum;
    }

}