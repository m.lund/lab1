package INF101.lab1.INF100labs;
import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multiplList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            multiplList.add(list.get(i)*2);
        }

        return multiplList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer value : list) {
            if (value % 3 != 0) {
                result.add(value);
            }
        }
        return result;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueList = new ArrayList <>();
        for (int elem : list) {
            if(!uniqueList.contains(elem)){
                uniqueList.add(elem);
            }
        }
        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}