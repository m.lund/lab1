package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("apple", "carrot", "ananas");
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int lenght1 = word1.length();
        int lenght2 = word2.length();
        int lenght3 = word3.length();

        if (lenght1 >= lenght2 && lenght1 >= lenght3) {
            System.out.println(word1);
        }
        if (lenght2 >= lenght1 && lenght2 >= lenght3) {
            System.out.println(word2);
        }
        if (lenght3 >= lenght1 && lenght3 >= lenght2) {
            System.out.println(word3);
        }

    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0 ) {
            if (year % 400 == 0) {
                return true;
            }
            if (year % 100 == 0) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num >0 && num % 2 == 0) {
            return true;
        }
        return false;
    }

}
